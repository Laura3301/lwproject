package StepDefinitions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LoginLogout {
	WebDriver driver = new ChromeDriver();
	@Given("^I am in the new tours login page\"(.*?)\"$")
	public void i_am_in_the_new_tours_login_page(String url) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		driver.get(url);
	   // throw new PendingException();
	}
	@When("^I enter username \"(.*?)\" and passwors\"(.*?)\"$")
	public void i_enter_username_and_passwors(String username, String password) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElement(By.name("userName")).sendKeys(username);
		driver.findElement(By.name("password")).sendKeys(password);
	    //throw new PendingException();
	}
	@When("^I press signin button$")
	public void i_press_signin_button() throws Throwable {
		driver.findElement(By.name("login")).click();
			//throw new PendingException();
	}

	@Then("^Signin should be successful and \"(.*?)\" link should be displayed$")
	public void signin_should_be_successful_and_link_should_be_displayed(String signoff) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    Assert.assertTrue(driver.findElement(By.linkText(signoff)).isDisplayed());
	    driver.close();
	    
		//throw new PendingException();
	}
}
