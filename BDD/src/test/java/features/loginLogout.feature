#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios 
#<> (placeholder)
#""
## (Comments)

#Sample Feature Definition Template
@tag
Feature: Test Login Page with correct credentials
	
@tag1
Scenario: Check login logout with username "a" and password "a"
Given I am in the new tours login page"http://www.newtours.demoaut.com/" 
When I enter username "a" and passwors"a"
And I press signin button
Then Signin should be successful and "SIGN-OFF" link should be displayed

@tag1
Scenario Outline: Check login test with correct credentials
Given I am in the new tours login page"http://www.newtours.demoaut.com/" 
When I enter username "<userName>" and passwors"<password>"
And I press signin button
Then Signin should be successful

Examples:
    | userName  |password | link    |
    | 5T        |  5      | SIGN-OFF|
    | B         |  B      | SIGN-OFF|
    |abc        | def     | SIGN-OFF|
    |lnk        |  7      | SIGN-ON |


#@tag2
#Scenario Outline: Title of your scenario outline
#Given I want to write a step with <name>
#When I check for the <value> in step
#Then I verify the <status> in step

